
public class Hello {

 public static void main(String[] args){
     System.out.println(sayHello("Testing Polaris123 !"));
     System.out.println(add(3, 4));
     System.out.println(substract(4, 3));
     System.out.println(divide(10, 0));
  System.out.println(pow(2, 2));
  // Another test comment
  /// Test Comment
     // Test comment 2
     // Test comment 3
     // Test comment 4
 }
 //Added comment: method for saying Hello to a name passed as parameter
 public static String sayHello(String name){ 
   return "Hello "+ name;
 }
 
  public static int add(int a, int b){ 
   return a+b;
 }
  public static int substract(int a, int b){ 
   return a-b;
 }
  public static int multiply(int a, int b){
   return a*b;
 }
  public static int divide(int a, int b) {
  if(b==0) {
   return 0;
  }
   return a/b;
 }
 public static int pow(int a, int b) {
   return (int)Math.pow(a, b);
 }
 
}